# Ticket Analysis

This repo contains notebooks that train models to predict a 'task' and an 'app' for a given ticket.

## Contents:

1. requirements.txt (file containing libraries used in this work)
2. app_dict.pickle (app dictionary containing list of apps and their synonyms)
3. task_dict_best.pickle (task dictionary containing list of tasks and their keywords)
4. df_raw.csv (list of raw tickets)
5. final_task_df.csv (curated dataframe for task model training and evaluation) 
6. task_model.joblib (model that predicts task)
7. app_model.joblib (model that predicts app)
8. task_label_encoder.joblib (decodes model output to human-readable task prediction) 
9. tfidf_vectorizer.joblib (converts new tickets to tfidf vectors)
10. app_classifier.ipynb (notebook containing app model work)
11. task_classifier.ipynb (notebook containing task model work)
12. combined.ipynb (notebook that acts as a demo predicting app and task tags for a small set of novel data) 

## Comments:

The app_classifier notebook takes raw tickets, tags them to create a dataset and then uses that dataset to 
train a model to predict apps. The task_classifier notebook does not contain code that generates the 
final_task_df.csv file, which is used to train the task model. This is because curating data for the task
model involved some manual work which cannot be reproduced by code. 


